'use strict';

var gulp = require('gulp');
var replace = require('gulp-replace');
var postcss = require('gulp-postcss');
var stylus = require('gulp-stylus');
var sourcemaps = require('gulp-sourcemaps');
var pixrem = require('pixrem');
var autoprefixer = require('autoprefixer-core');
var browsersync = require('browser-sync');
var config = require('../config');
var plumber = require('gulp-plumber');
var filter = require('gulp-filter');

var postCssStack = [autoprefixer()];

if (config.ie8Support) postCssStack.unshift(pixrem());

gulp.task('stylus', function () {
    return gulp.src(config.src.stylus)
        .pipe(plumber())
        .pipe(stylus(config.stylus))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(
            postcss(postCssStack)
        )
        .pipe(sourcemaps.write(/*'.',{
            includeContent: false,
            sourceRoot: '.'
        }*/))
        .pipe(gulp.dest(config.dest.css))
        .pipe(browsersync.reload({stream: true}))
});

//gulp.task('stylus', ['origStylus'], function () {
//    return gulp.src(config.dest.css + 'app.css.map')
//        .pipe(replace(/(.+?)src\/(.+?)/gim, '$1$2'))
//        .pipe(gulp.dest(config.dest.css));
//});
