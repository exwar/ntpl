'use strict';

var gulp = require('gulp');
var config = require('../config');
var svgSprite = require('gulp-svg-sprites');
var svg2png = require('gulp-svg2png');
var filter = require('gulp-filter');

gulp.task('sprites', function () {
    return gulp.src(config.src.svg)
        //.pipe(svg2png())
        //.pipe(gulp.dest("src/jade/symbols/1"))
        .pipe(svgSprite({
            //mode: "symbols",
            //preview: false,

            cssFile: '_symbols.styl',

            svg: {
                sprite: '_symbols.svg'
            }
        }))

        .pipe(gulp.dest("src/jade/symbols"))

        .pipe(filter("**/*.svg"))
        ;
});