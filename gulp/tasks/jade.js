'use strict';

var gulp = require('gulp');
var jade = require('gulp-jade');
var jadeInheritance = require('gulp-jade-inheritance');
var browserSync = require('browser-sync');
var config = require('../config');
var changed = require('gulp-changed');
var cached = require('gulp-cached');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var filter = require('gulp-filter');

var filterJade = function(file) {
    return !/\/_/.test(file.path) || !/^_/.test(file.relative)
};

gulp.task('jade', [], function() {
    return gulp.src(config.src.jade)
        .pipe(plumber())
        .pipe(changed(config.dest.html, {extension: '.html'}))
        .pipe(gulpif(global.isWatching, cached('jade')))
        .pipe(jadeInheritance({ basedir: config.src.jadeDir }))
        .pipe(filter(filterJade))
        .pipe(jade({ pretty: true }))
        .pipe(gulp.dest(config.dest.html))
        .pipe(browserSync.reload({ stream: true }));
});