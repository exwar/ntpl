'use strict';

var gulp = require('gulp');
var config = require('../config');
var modernizr = require('gulp-modernizr');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');

gulp.task('modernizr', function() {
    return gulp.src(config.src.modernizr)
        .pipe(plumber())
        .pipe(modernizr(config.modernizr))
        .pipe(gulpif(!config.isDebug, uglify()))
        .pipe(gulp.dest(config.dest.js));
});
