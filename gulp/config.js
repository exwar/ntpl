'use strict';

var SRCDIR = './src';
var DESTDIR = './build';

var ie8Support = false;
var isDebug = true;

var src = {
    'jadeDir': SRCDIR + '/jade',
    'jade': [SRCDIR + '/jade/**/*.jade'],

    'stylusWatch': [SRCDIR + '/stylus/**/*.styl'],
    'stylus': [SRCDIR + '/stylus/*.styl', '!' + SRCDIR + '/stylus/_*.styl'],

    'svg': [SRCDIR + '/images/svg/**.svg'],

    'js': [SRCDIR + '/scripts/app.js'],

    'modernizr': [DESTDIR + '/assets/js/app.js', DESTDIR + '/assets/css/app.css'],

    'gulpWatch': ['./gulp/**/*.js']
};

var dest = {
    'html': DESTDIR + '/_html/',
    'css': DESTDIR + '/assets/css/',
    'js': DESTDIR + '/assets/js/'
};

var exports = {
    'src': src,
    'dest': dest,

    'ie8Support': ie8Support,
    'isDebug': isDebug,

    'stylus': {
        'url': {
            'name': 'embedurl',
            'paths': [SRCDIR + '/images'],
            'limit': false
        },

        sourcemap: {
            inline: true,
            sourceRoot: '.',
            basePath: dest.css
        }
    },

    'browserSync': {
        //notify: true,
        open: 'external',
        //open: false,

        files: [
            DESTDIR + '/**',
            '!' + DESTDIR + '/**/*.css.map'
        ],

        startPath: '/_html/start.html',

        server: {
            baseDir: [DESTDIR, SRCDIR],
            directory: true
        }
    },

    modernizr: {
        "cache": true,

        // Based on default settings on http://modernizr.com/download/
        "options" : [
            "setClasses",
            "addTest",
            "html5printshiv",
            "testProp",
            "fnBind"
        ],

        // Useful for excluding any tests that this tool will match
        // e.g. you use .notification class for notification elements,
        // but don’t want the test for Notification API
        "excludeTests": [],

        // Have custom Modernizr tests? Add them here.
        "customTests" : []
    },

    browserify: {
        debug: isDebug,
        // Additional file extentions to make optional
        extensions: [],

        // A separate bundle will be generated for each
        // bundle config in the list below
        bundleConfigs: [{
            entries: src.js,
            dest: dest.js,
            outputName: 'app.js'
        }]
    }
};

module.exports = exports;