'use strict';

var $;
var Backbone = require('backbone');
$ = Backbone.$ = require('jquery');

var View = require('./modules/default');

$(function() {
   var view = new View({
       el: '.js-view-example'
   });
});