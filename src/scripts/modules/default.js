var Backbone = require('backbone');

module.exports = Backbone.View.extend({
    events: {
        'click .js-view-example-button': 'buttonClick'
    },

    buttonClick: function() {
        console.log('Smack my button again!');
    }
});